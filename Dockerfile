FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > tmux.log'

COPY tmux .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' tmux
RUN bash ./docker.sh

RUN rm --force --recursive tmux
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD tmux
